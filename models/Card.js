const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
  user_id: { type: Types.ObjectId, ref: 'User' },
  name: { type: String },
  description: { type: String },
  src: { type: String, required: true },
  cost: { type: Number, default: 0 },
  like: { type: Number, default: 0 },
  active: { type: Boolean, default: false },
  moderated: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  moderate_msg: { type: String, default: '' },
  ts: { type: Date, default: Date.now },
})

module.exports = model('Card', schema)