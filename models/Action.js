const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
  user_id: { type: Types.ObjectId, ref: 'User' },
  card_id: { type: Types.ObjectId, ref: 'Card' },
  performed: { type: Boolean, default: false },
  moderated: { type: Boolean, default: false },
  sent: { type: Boolean, default: false },
  attachments: [{ type: String }],
  message_in: { type: String },
  message_out: { type: String },
  deleted: { type: Boolean, default: false },
  ts_update: { type: Date },
  ts: { type: Date, default: Date.now },
})

module.exports = model('Action', schema)