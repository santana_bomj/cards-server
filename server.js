const express = require('express');
const cluster = require('cluster');
const http = require('http');
const formData = require("express-form-data");
const os = require("os");
const fs = require('fs');
const mongoose = require('mongoose')
var checkUser = require('./checkUser')
const User = require('./models/User')
const Card = require('./models/Card')
const Action = require('./models/Action')
const Shop = require('./models/Shop')
const Purchases = require('./models/Purchases')
const Config = require('./config')
var cpuCount = require('os').cpus().length;
var workers = [];

const DB_CONNECT = Config.DB_CONNECT
const PORT = process.env.PORT || 80

async function start() {
    try {
        await mongoose.connect(DB_CONNECT, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        if (cluster.isMaster) {
            for (var i = 0; i < cpuCount; i += 1) {
                var worker = cluster.fork();
                worker.on('message', function (data) {
                    for (var j in workers) { workers[j].send(data); }
                });
                workers.push(worker);
            }
            cluster.on('exit', (worker, code, signal) => {
                console.log(`worker ${worker.process.pid} died`);
                cluster.fork();
            });
        }


        if (cluster.isWorker) {
            var app = express();
            const options = {
                uploadDir: os.tmpdir(),
                autoClean: true,
                maxFieldsSize: '50mb'
            };
            app.use(formData.parse(options));
            app.use(formData.format());
            app.use(formData.stream());
            app.use(formData.union());

            app.use(function (req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                next();
            });

            app.use(function (req, res, next) {
                if (!checkUser(req.body))
                    res.json({ status: 'error', message: 'Ошибка авторизации' })
                else next()
            });

            app.post('/', async (request, response) => {
                try {
                    var newUser = false
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        user = new User({ vk_id: request.body.vk_user_id, name: decodeURIComponent(request.body.name), avatar: request.body.avatar })
                        newUser = true
                        await user.save()
                    }

                    var like = await Card.aggregate([{ $match: { user_id: mongoose.Types.ObjectId(user._id), active: true, deleted: false } }, { $group: { _id: null, count: { $sum: "$like" } } }])
                    var data = {
                        cards: user.cards.length,
                        balance: user.balance,
                        authorRating: like.length > 0 ? like[0].count : 0,
                        actions: user.actions.length
                    }

                    var existingCards = user.actions
                    var cards = await Card.aggregate([{ $match: { '_id': { '$nin': existingCards }, active: true, deleted: false } }, { $sample: { size: 5 } }, { $unset: ['active', 'moderated', 'deleted', 'moderate_msg', 'user_id', 'ts', '__v'] }])
                    var action_data = await Action.aggregate([
                        {
                            $match: {
                                user_id: user._id,
                                deleted: false,
                                performed: true
                            }
                        },
                        {
                            $lookup:
                            {
                                from: "cards",
                                localField: "card_id",
                                foreignField: "_id",
                                as: "card"
                            }
                        }
                    ])

                    var performedCards = []
                    action_data.forEach(item => {
                        performedCards.push({ likeState: true, cost: item.card[0].cost, sent: item.sent, _id: item._id, name: item.card[0].name, description: item.card[0].description, src: item.card[0].src })
                    })

                    response.json({ status: 'ok', cards, data, performedCards, newUser })
                    if (decodeURIComponent(request.body.user_name) !== '' && decodeURIComponent(request.body.user_name) !== user.name)
                        user.name = decodeURIComponent(request.body.user_name)
                    if (request.body.avatar !== '' && request.body.avatar !== user.avatar)
                        user.avatar = request.body.avatar
                    user.save()
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/toggleCard', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }

                    var action = await Action.findOne({ 'user_id': user._id, 'card_id': mongoose.Types.ObjectId(request.body._id) })
                    var card = await Card.findOne({ _id: mongoose.Types.ObjectId(request.body._id) });
                    if (action) {
                        console.log(card.like)
                        card.like = card.like - (action.deleted ? -1 : 1)
                        console.log(card.like)
                        action.deleted = !action.deleted
                        user.actions = user.actions.filter(item => item !== request.body._id)
                    } else {
                        card.like = card.like + 1
                        action = new Action({
                            card_id: mongoose.Types.ObjectId(request.body._id),
                            user_id: user._id,
                        })
                        user.actions.push(request.body._id)
                    }
                    await action.save()
                    await user.save()
                    await card.save()
                    response.json({ status: 'ok' })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/toggleFavoriteCard', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var action = await Action.findOne({ 'user_id': user._id, '_id': mongoose.Types.ObjectId(request.body._id) })
                    if (!action) {
                        response.json({ status: 'error', message: 'Ошибка' })
                        return false
                    }
                    var card = await Card.findOne({ _id: action.card_id });
                    if (!action.deleted) {
                        card.like = card.like - 1
                        action.deleted = !action.deleted
                        user.actions = user.actions.filter(item => item.toString() !== action.card_id.toString())
                    } else {
                        card.like = card.like + 1
                        action.deleted = !action.deleted
                        user.actions.push(action.card_id)
                    }

                    await action.save()
                    await user.save()
                    await card.save()
                    response.json({ status: 'ok' })
                }
                catch (e) {
                    console.log(e)
                }
            });
            app.post('/getFavorites', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var action_data = await Action.aggregate([
                        {
                            $match: {
                                user_id: user._id,
                                deleted: false,
                                performed: false
                            }
                        },
                        {
                            $lookup:
                            {
                                from: "cards",
                                localField: "card_id",
                                foreignField: "_id",
                                as: "card"
                            }
                        }
                    ])

                    var data = []
                    action_data.forEach(item => {
                        data.push({ likeState: true, cost: item.card[0].cost, sent: item.sent, _id: item._id, name: item.card[0].name, description: item.card[0].description, moderated: item.moderated, attachments: item.attachments, message_in: item.card[0].message_in, message_out: item.message_out, src: item.card[0].src })
                    })

                    response.json({ status: 'ok', data })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/upload', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user || user.actions.filter(item => item.toString() === request.body._id) === 0) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }

                    var action = await Action.findOne({ '_id': mongoose.Types.ObjectId(request.body._id) })
                    var filePaths = []
                    var fileUrl = []
                    request.body.file.forEach(item => {
                        if (item.startsWith('https://'))
                            fileUrl.push(item)
                        else {
                            var tmp_name = Date.now()
                            var pathFile = './img/' + tmp_name + '.png'
                            var imgdata = item;
                            var base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
                            fs.writeFileSync(pathFile, base64Data, { encoding: 'base64' });
                            filePaths.push(pathFile)
                        }
                    })
                    var vk = null;
                    var vkapi = require("easyvk")
                    await vkapi({
                        access_token: request.body.token
                    }).then(_vk => {

                        vk = _vk;
                    })

                    var wait = filePaths.map(async (item) => {
                        return vk.uploader.getUploadURL('photos.getUploadServer', {
                            group_id: '144239305',
                            album_id: '270698806'
                        })
                            .then(async ({ vk, url, vkr }) => {
                                const field = 'file1'
                                const server = vk.uploader
                                const filePath = item
                                let { vkr: fileData } = await (server.uploadFile(url, filePath, field, {}))
                                fileData = await (vk.call('photos.save', { fileData, album_id: 270698806, group_id: 144239305, photos_list: fileData.photos_list, server: fileData.server, hash: fileData.hash }))
                                fileData = fileData.vkr[0]
                                fileUrl.push(fileData[Object.keys(fileData)[Object.keys(fileData).length - 5]])
                                fs.unlinkSync(item)
                            })
                            .catch((error) => {
                                console.log(error)
                                response.json({ status: 'error' })
                                return
                            })
                    })

                    Promise.all(wait).then(async () => {
                        action.attachments = fileUrl
                        action.sent = true
                        action.ts_update = Date.now()
                        await action.save()
                        response.json({ status: 'ok' })
                    });

                } catch (e) {
                    console.log(e)
                    response.json({ status: 'error' })
                    return
                }
            });

            app.post('/uploadTask', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user || user.actions.filter(item => item.toString() === request.body._id) === 0) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }

                    const tmp_name = Date.now()
                    const path = require('path')
                    const pathFile = './img/' + tmp_name + '.png'
                    const imgdata = request.body.file;
                    const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
                    fs.writeFileSync(pathFile, base64Data, { encoding: 'base64' });
                    var easyvk = require("easyvk")
                    easyvk({
                        access_token: request.body.token,
                    })
                        .then(async (vk) => {
                            return vk.uploader.getUploadURL('photos.getUploadServer', {
                                group_id: '144239305',
                                album_id: '270698806'
                            })
                                .then(async ({ vk, url, vkr }) => {
                                    const field = 'file1'
                                    const server = vk.uploader
                                    const filePath = path.join(__dirname, '/img/' + tmp_name + '.png')
                                    let { vkr: fileData } = await (server.uploadFile(url, filePath, field, {}))
                                    fileData = await (vk.call('photos.save', { fileData, album_id: 270698806, group_id: 144239305, photos_list: fileData.photos_list, server: fileData.server, hash: fileData.hash }))
                                    fileData = fileData.vkr[0]
                                    let src = fileData[Object.keys(fileData)[Object.keys(fileData).length - 5]]

                                    card = new Card({ user_id: user._id, name: decodeURIComponent(request.body.name), description: decodeURIComponent(request.body.description), src })
                                    await card.save()
                                    user.cards.push(card._id)
                                    await user.save()

                                    fs.unlinkSync('./img/' + tmp_name + '.png')
                                    response.json({ status: 'ok' })
                                })
                                .catch((error) => {
                                    console.log(error)
                                    response.json({ status: 'error' })
                                });
                        })
                        .catch((error) => {
                            console.log(error)
                            response.json({ status: 'error' })
                        });

                } catch (e) {
                    console.log(e)
                }
            });

            app.post('/shop', async (request, response) => {
                try {
                    let shop = await Shop.find({ active: true })
                    response.json({ status: 'ok', shop })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/authorRating', async (request, response) => {
                try {
                    let author_rating = JSON.parse(fs.readFileSync('author_rating.json', 'utf8'));
                    response.json({ status: 'ok', data: author_rating })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/authorWork', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user || user.actions.filter(item => item.toString() === request.body._id) === 0) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }

                    let cards = await Card.find({ user_id: user._id, deleted: false });
                    response.json({ status: 'ok', data: cards })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/deleteWork', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user || user.actions.filter(item => item.toString() === request.body._id) === 0) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }

                    var card = await Card.findOne({ _id: request.body._id, user_id: user._id });
                    card.deleted = true
                    user.cards = user.cards.filter(item => item.toString() !== request.body._id.toString())
                    await card.save()
                    await user.save()

                    let cards = await Card.find({ user_id: user._id, deleted: false });
                    response.json({ status: 'ok', data: cards })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/buy', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user || user.actions.filter(item => item.toString() === request.body._id) === 0) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var shop = await Shop.findOne({ _id: request.body._id })
                    var price = shop.price
                    price = parseInt(price)
                    if (shop.active && user.balance >= price) {
                        purchases = new Purchases({
                            shop_id: mongoose.Types.ObjectId(request.body._id),
                            balance_before: user.balance,
                            type: shop.type
                        })
                        await purchases.save()
                        user.balance = user.balance - price

                        var temp_purchases = user.purchases.get(request.body._id)
                        if (!temp_purchases) {
                            user.purchases.set(request.body._id, [{
                                ts: Date.now()
                            }])
                        }
                        else {
                            temp_purchases.push({
                                ts: Date.now(),
                            })
                            user.purchases.set(request.body._id, temp_purchases)
                        }
                    } else {
                        response.json({ status: 'error', message: 'Ошибка' })
                        return false
                    }

                    user.save()
                    response.json({ status: 'ok', data: { balance: user.balance } })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/getCards', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user || user.actions.filter(item => item.toString() === request.body._id) === 0) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var existingCards = JSON.parse(request.body.exist).map(function (el) { return mongoose.Types.ObjectId(el) });
                    var cards = await Card.aggregate([{ $match: { '_id': { '$nin': [...existingCards, ...user.actions] }, active: true, deleted: false } }, { $sample: { size: 5 } }, { $unset: ['active', 'moderated', 'deleted', 'moderate_msg', 'user_id', 'ts', '__v'] }])
                    console.log(existingCards, cards)
                    response.json({ status: 'ok', cards })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/getUser', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user || user.actions.filter(item => item.toString() === request.body._id) === 0) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var active_user
                    if (mongoose.Types.ObjectId.isValid(request.body._id)) {
                        active_user = await User.findOne({ _id: request.body._id });
                        if (!active_user) {
                            response.json({ status: 'ok', user: null })
                            return false
                        }
                    }

                    var like = await Card.aggregate([{ $match: { user_id: mongoose.Types.ObjectId(active_user._id), active: true, deleted: false } }, { $group: { _id: null, count: { $sum: "$like" } } }, { $unset: ['active', 'moderated', 'deleted', 'moderate_msg', 'user_id', 'ts', '__v'] }])
                    var data = {
                        cards: active_user.cards.length,
                        balance: active_user.balance,
                        authorRating: like[0].count,
                    }
                    var action_data = await Action.aggregate([
                        {
                            $match: {
                                user_id: active_user._id,
                                deleted: false,
                                performed: true
                            }
                        },
                        {
                            $lookup:
                            {
                                from: "cards",
                                localField: "card_id",
                                foreignField: "_id",
                                as: "card"
                            }
                        }
                    ])

                    var performedCards = []
                    action_data.forEach(item => {
                        performedCards.push({ like: (user.actions.includes(item._id) ? true : false), cost: item.card[0].cost, _id: item._id, name: item.card[0].name, description: item.card[0].description, src: item.card[0].src })
                    })
                    response.json({ status: 'ok', user: { ...data, name: decodeURIComponent(active_user.name), avatar: decodeURIComponent(active_user.avatar), performedCards } })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/getRating', async (request, response) => {
                try {
                    let rating = JSON.parse(fs.readFileSync('rating.json', 'utf8'));
                    response.json({ status: 'ok', data: rating })
                }
                catch (e) {
                    console.log(e)
                }
            });

            http.createServer(app).listen(PORT, function () {
                console.log("Express server listening on port " + PORT + " as Worker " + cluster.worker.id + " running @ process " + cluster.worker.process.pid + "!");
            });
        }
    } catch (e) {
        console.log('Server Error', e.message)
        process.exit(1)
    }
}

start()
