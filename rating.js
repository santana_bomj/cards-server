
const fs = require('fs');
const mongoose = require('mongoose')
const Config = require('./config')
const Card = require('./models/Card')
const User = require('./models/User')

let DB_CONNECT = Config.DB_CONNECT

//setInterval
start()

async function start() {
    try {
        console.log('start rating')
        await mongoose.connect(DB_CONNECT, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        var card_rating = []
        var cards = await Card.aggregate([
            {
                $match: {
                    user_id: { $ne: null }
                }
            },
            {
                $group: {
                    _id: "$user_id",
                    count: {
                        $sum: "$like"
                    }
                }
            },
            { $sort: { count: -1 } },
            { $limit: 13 },
            {
                $lookup:
                {
                    from: "users",
                    localField: "_id",
                    foreignField: "_id",
                    as: "user"
                }
            }
        ])
        cards.forEach(card => {
            card_rating.push({ _id: card._id, rating: card.count, name: card.user[0].name, avatar: card.user[0].avatar })
        });
        console.log(card_rating)
        fs.writeFile('author_rating.json', JSON.stringify(card_rating), 'utf8', () => { });
        var user_rating = []
        var users = await User.aggregate([
            { $sort: { balance: -1 } },
            { $limit: 13 },
        ])

        users.forEach(user => {
            user_rating.push({ _id: user._id, rating: user.balance, name: user.name, avatar: user.avatar })
        })
        console.log(user_rating)
        fs.writeFile('rating.json', JSON.stringify(user_rating), 'utf8', () => { });


        console.log('end rating')
        mongoose.disconnect();
    } catch (e) {
        console.log('Server Error', e.message)
        process.exit(1)
    }
}